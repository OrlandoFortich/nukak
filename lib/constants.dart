import 'package:flutter/material.dart';

const kPrimaryColor = Color.fromRGBO(168, 84, 27, 1);
const kPrimaryLightColor = Color(0xFFF1E6FF);
